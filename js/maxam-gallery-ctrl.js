(function(){
    var app = angular.module('maxam-gallery-ctrl', [ ]);

    app.controller('MaxamGalleryController', ['$http', function($http) {
        var vm = this;
        vm.data = {};

        vm.data.images_count = 0;

        $http.get('json/gallery-data.json').success(function(data) {
            vm.data = data;
        });

        vm.has_data = function() {
            return vm.data.images_count != 0;
        };

        vm.sources = function() {
            var result = [];
            for(var i = 1; i <= vm.data.images_count; ++i) {
                result.push(vm.data.base_path + i + vm.data.ext);
            }
            return result;
        };
    }]);

})();