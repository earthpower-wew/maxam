(function(){
    var app = angular.module('maxam-header-ctrl', [ ]);

    app.controller('MaxamHeaderController', ['$http', function($http) {
        var header = this;
        header.data = [];

        $http.get('json/header-data.json').success(function(data) {
            header.data = data;
        });
    }]);
    
})();