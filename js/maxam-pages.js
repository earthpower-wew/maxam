(function(){
    var app = angular.module('maxam-pages', []);

    app.directive('pageCompany', function() {
        return {
            restrict: 'E',
            templateUrl: 'pages/company.html'
        };
    });

    app.directive('pageRestaurant', function() {
        return {
            restrict: 'E',
            templateUrl: 'pages/restaurant.html'
        };
    });

    app.directive('pageHotel', function() {
        return {
            restrict: 'E',
            templateUrl: 'pages/hotel.html'
        };
    });

    app.directive('pagePetrolStation', function() {
        return {
            restrict: 'E',
            templateUrl: 'pages/petrol-station.html'
        };
    });

    app.directive('pageContact', function() {
        return {
            restrict: 'E',
            templateUrl: 'pages/contact.html'
        };
    });
})();