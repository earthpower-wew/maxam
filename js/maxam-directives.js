(function(){
    var app = angular.module('maxam-directives', ['maxam-header-ctrl', 'maxam-gallery-ctrl']);

    app.directive('maxamHeader', function() {
        return {
            restrict: 'E',
            templateUrl: 'view/maxam-header.html'
        };
    });

    app.directive('maxamFooter', function() {
        return {
            restrict: 'E',
            templateUrl: 'view/maxam-footer.html'
        };
    });

    app.directive('gallery', function() {
        return {
            restrict: 'E',
            templateUrl: 'view/gallery.html'
        };
    });
})();